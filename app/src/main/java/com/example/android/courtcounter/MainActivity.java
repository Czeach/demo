package com.example.android.courtcounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int scoreTeamA = 0;
    int scoreTeamB = 0;

    /**
     *
     * This method displays the score for team A
     */
    public void displayForTeamA (int score){
        TextView scoreView = (TextView) findViewById(R.id.team_A_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     *
     * This method displays the score for team B
     */
    public void displayForTeamB (int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_B_score);
        scoreView.setText (String.valueOf(score));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeamA(scoreTeamA);
        displayForTeamB(scoreTeamB);
    }

    /**
     *
     * This method is called when the +3 button is clicked in team A
     */
    public void TeamA3Points (View view) {
        scoreTeamA = scoreTeamA + 3;
        displayForTeamA(scoreTeamA);
    }

    /**
     *
     * This method is called when the +2 button is clicked in team A
     */
    public void TeamA2Points (View view) {
        scoreTeamA = scoreTeamA + 2;
        displayForTeamA(scoreTeamA);
    }

    /**
     *
     * This method is called when the free throw button is clicked in team A
     */
    public void TeamAFreeThrowPoints (View yiew) {
        scoreTeamA = scoreTeamA + 1;
        displayForTeamA(scoreTeamA);
    }

    /**
     *
     * This method is called when the +3 button is clicked in team B
     */
    public void TeamB3Points (View view) {
        scoreTeamB = scoreTeamB + 3;
        displayForTeamB(scoreTeamB);
    }

    /**
     *
     * This method is called when the +2 button is clicked in team B
     */
    public void TeamB2Points (View view) {
        scoreTeamB = scoreTeamB + 2;
        displayForTeamB(scoreTeamB);
    }

    /**
     *
     * This method is called when the free throw button is clicked in team B
     */
    public void TeamBFreeThrowPoints (View view) {
        scoreTeamB = scoreTeamB + 1;
        displayForTeamB(scoreTeamB);
    }

    public void Reset (View view) {
        scoreTeamA = scoreTeamA - scoreTeamA;
        scoreTeamB = scoreTeamB - scoreTeamB;
        displayForTeamA(scoreTeamA);
        displayForTeamB(scoreTeamB);

    }
}

